/*
    Set the application version
*/
import Vue from 'vue'

Vue.prototype.$version = "v2020.05.07.1";
